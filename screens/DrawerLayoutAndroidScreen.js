import React, { Component } from 'react';
import { View, DrawerLayoutAndroid, Text } from 'react-native';

export default class DrawerLayoutAndroidScreen extends Component {
    render() {
        var navigationView = (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Text style={{ margin: 10, fontSize: 15, textAlign: 'left' }}>Hi, I'm the drawer! :)</Text>
            </View>
        );
        return (
            <DrawerLayoutAndroid
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>Hello</Text>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>World!</Text>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>-------------></Text>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>Drag left-right to show drawer</Text>
                </View>
            </DrawerLayoutAndroid>
        );
    }
}

